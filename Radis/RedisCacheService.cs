﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Text.Json;

namespace MusicGanFE.Radis
{
    public class RedisCacheService : IRedisCacheService
    {
        private readonly IDistributedCache _cache;

        public RedisCacheService(IDistributedCache cache)
        {
            _cache = cache;
        }

        public T Get<T>(string key)
        {
            var value = _cache.GetString(key);

            if (value != null)
            {
                return JsonSerializer.Deserialize<T>(value);
            }

            return default;
        }

        public T Set<T>(string key, T value)
        {
            /**
            * Dữ liệu sẽ được vào RAM sẽ được lưu nhớ trong khoảng 24 tiếng, sau khi hết 24 tiếng sẽ chuyển dữ liệu vào SlidingExpiration ở dưới
            * SlidingExpiration là 60p nếu dữ liệu không được truy cập trong 60p thì nó sẽ tự động xóa khỏi bộ nhớ đệm
            */
            var timeOut = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(24),
                SlidingExpiration = TimeSpan.FromMinutes(60)
            };

            _cache.SetString(key, JsonSerializer.Serialize(value), timeOut);

            return value;
        }

    }
}
