﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using MusicGanFE.Radis;
using System.Text.Encodings.Web;
using System.Text.Unicode;
namespace MusicGanFE
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(opt => { opt.ResourcesPath = "Resources"; });
            services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddSession();
            services.AddSingleton(HtmlEncoder.Create(allowedRanges: new[] { UnicodeRanges.All }));
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            // Lấy dữ liệu về IP
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //Fix upload file nặng
            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });
            // Add RabibitMQ by Hoan Chu 13/01/2023
            //services.AddRawRabbit(new RawRabbitOptions
            //{
            //    ClientConfiguration = new RawRabbit.Configuration.RawRabbitConfiguration
            //    {
            //        Username = "guest",
            //        Password = "guest",
            //        VirtualHost = "/",
            //        Port = 5672,
            //        Hostnames = new List<string> { "localhost" },
            //        RequestTimeout = TimeSpan.FromSeconds(10),
            //        PublishConfirmTimeout = TimeSpan.FromSeconds(1),
            //        RecoveryInterval = TimeSpan.FromSeconds(1),
            //        PersistentDeliveryMode = true,
            //        AutoCloseConnection = true,
            //        AutomaticRecovery = true,
            //        TopologyRecovery = true,
            //        Exchange = new RawRabbit.Configuration.GeneralExchangeConfiguration
            //        {
            //            Durable = true,
            //            AutoDelete = false,
            //            Type = RawRabbit.Configuration.Exchange.ExchangeType.Topic
            //        },
            //        Queue = new RawRabbit.Configuration.GeneralQueueConfiguration
            //        {
            //            Durable = true,
            //            AutoDelete = false,
            //            Exclusive = false
            //        }
            //    }
            //});

            // Add Radis
            services.AddControllers();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "SampleInstance";
            });
            services.AddSingleton<IRedisCacheService, RedisCacheService>();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = $"{Configuration.GetValue<string>("RedisCache:Host")}:{Configuration.GetValue<int>("RedisCache:Port")}";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // to get ip address
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            //app.UseHttpsRedirection(); // Enable https
            app.UseStaticFiles();
            // Cho phép sử dụng session
            app.UseSession();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            // Đa ngôn ngữ
            var supportedCultures = new[] { "en", "vi", "zh" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);
            app.UseRequestLocalization(localizationOptions);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.Run(context =>
            //{
            //    context.Response.Redirect("/404");
            //    return Task.FromResult(0);
            //});
        }
    }
}