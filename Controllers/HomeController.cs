﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicGanFE.Models;
using MusicGanFE.Radis;
using System.Collections.Generic;
using System.Diagnostics;

namespace MusicGanFE.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRedisCacheService _redisCache;

        public HomeController(ILogger<HomeController> logger, IRedisCacheService redisCache)
        {
            _logger = logger;
            _redisCache = redisCache;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [Route("/Home/Last")]
        [HttpGet]
        public string GetLastest()
        {
            Dictionary<object, object> dict = new();
            string data = System.IO.File.ReadAllText("/home/amnhacsaigon/dotnet/musicfe/wwwroot/mp3/last.txt");
            return data;
        }

        public object GetDataFromRadis(string file)
        {
            var cacheId = _redisCache.Get<string>(file);
            if (cacheId != null) { return cacheId; }
            else
            {

            }
            return "";
        }
    }
}
